import datetime
import logging
from .findlocation import in_NSW, NSW_REDIRECT_IDS
import os
from ipware.ip import get_real_ip, get_ip

from django.conf import settings
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.contrib.auth.views import redirect_to_login
from django.contrib.sites.models import Site
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.urls import reverse
from django.db.models import Q
from django.http import Http404, HttpResponseForbidden
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView
from django.views.generic.list import ListView

from .forms import ItemEditForm, \
    MemberForm, ARCHIVE_OPTION, UserForm
from .models import Item, Member, \
    TYPE_ID_TAB, TYPE_ID_SECTION, TYPE_ID_TOPIC, \
    ITEM_ID_HOME

MAX_ITEMS_PER_PAGE = 50

STAFF_LOGIN_URL = settings.STAFF_LOGIN_URL

logger = logging.getLogger(__name__)

def get_staff_buttons_action(request, item):
    """Get edit and add form actions for staff """
    if request.user.is_staff:
        edit_action = '/edit/%d/' % item.item_id
        add_action = '/add/%d/' % item.item_id
    else:
        edit_action = add_action = ''
    return(edit_action, add_action)


def search(request):
    """Search website pages.

    TODO: Breadcrumb generation is expensive (see template)
    TODO: Replace with a search library, eg. Haystack

    """
    # Shortcut for rider search
    if (request.user.is_staff and 'q' in request.GET
        and request.GET['q'].isdigit()):
        return redirect('/rider/?id=%s' % request.GET['q'])

    phrase = ''
    results = []

    if 'q' in request.GET:
        phrase = request.GET['q']
        results = Item.objects.search(phrase)
    else:
        results = []

    return render(request, 'vanilla/search.html',
                  {'objects': results,
                   'phrase': phrase})


@login_required(login_url=STAFF_LOGIN_URL)
@user_passes_test(lambda u: u.is_staff, login_url=STAFF_LOGIN_URL)
def create_item(request, parent_item_id):
    """Create a new page

    TODO: Currently hacked together just so we can get out of
    PHP. Needs a review.
    """
    parent = get_object_or_404(Item, pk=parent_item_id)
    can_add = parent.can_add_below(request.user)
    can_publish = request.user.has_perm('vanilla.publish_item')
    if not can_add:
        return HttpResponseForbidden("Sorry, you are not allowed to add a page here.")

    if parent.type_id < TYPE_ID_TAB or parent.type_id > TYPE_ID_TOPIC:
        return HttpResponseForbidden("Sorry, you can't add a page at this level.")

    if request.method == 'POST':
        # Edits cancelled
        if 'cancel' in request.POST:
            return redirect(parent.get_absolute_url())

        # Edits saved

        # NOTE: if form doesn't validate, the variable will get blown
        # away by the later created form. That's a potential problem.
        form = ItemEditForm(can_publish, data=request.POST, files=request.FILES)
        if form.is_valid():
            item = form.save(commit=False)
            item.relate = parent
            item.type_id = parent.type_id + 1
            item.save()
            status = dict(form.fields['status'].choices)[int(form.cleaned_data['status'])]
            item.add_change_log(request.user, form.cleaned_data["change_summary_append"], 'Add new page', status)
            return redirect(item.get_absolute_url())

    item = Item()

    item.user = request.user

    # Update dates for use in form
    item.publish_date = datetime.date.today()
    item.review_date = datetime.date.today() + datetime.timedelta(days=180)
    form = ItemEditForm(can_publish, instance=item)

    # Only live staff will be listed
    form.fields['user'].queryset = User.objects.filter(is_staff=True, is_active=True).order_by('first_name', 'last_name')

    return render(request, 'vanilla/item_form.html',
                  {'form': form,
                   'page_title': 'Add',
                  })


@login_required(login_url=STAFF_LOGIN_URL)
@user_passes_test(lambda u: u.is_staff, login_url=STAFF_LOGIN_URL)
def update_item(request, item_id):
    """
    Generates the editor textareas and saves page content:
     - sub-section, topic and page don't get sub-content-a editor
     - confirmation before archiving is handled with Javascript
     - locks page during editing
     - staff see draft version if available
     - editor upload is done with a PHP program

    TODO: nicer 403
    """
    item = get_object_or_404(Item, pk=item_id)
    can_edit = item.can_edit(request.user, request.session.session_key)
    can_publish = item.can_publish(request.user)
    can_archive = item.can_archive(request.user)
    if not can_edit:
        return HttpResponseForbidden("Sorry, you are not allowed to edit this page.")

    item.query_lock(request.session.session_key)
    if item.has_lock() and not item.is_locked():
        return HttpResponseForbidden("Sorry, this page is locked for editing.")

    if request.method == 'POST':
        # Edits cancelled
        if 'cancel' in request.POST:
            item.release_lock()
            return redirect(item.get_absolute_url())

        # Edits saved

        # The form dynamically adds the "archive" option to the status
        # field. We don't need to check if someone is trying to
        # archive when they shouldn't - the form won't validate.
        form = ItemEditForm(can_publish, can_archive, data=request.POST, files=request.FILES, instance=item)
        if form.is_valid():
            item = form.save()
            status = dict(form.fields['status'].choices)[int(form.cleaned_data['status'])]
            item.add_change_log(request.user, form.cleaned_data["change_summary_append"], 'Edit page', status)
            item.release_lock()
            return redirect(item.get_absolute_url())

    # Lock page
    author_name = "%s %s" % (request.user.first_name, request.user.last_name)
    item.claim_lock(author_name, request.session.session_key)

    # Update dates for use in form
    item.publish_date = datetime.date.today()
    item.review_date = datetime.date.today() + datetime.timedelta(days=180)
    form = ItemEditForm(can_publish, can_archive, instance=item)

    # Only live staff will be listed, but with an exception for the
    # author of the page even if they're not live.
    form.fields['user'].queryset = User.objects.filter(is_staff=True).filter(Q(is_active=True) | Q(id=item.user_id)).order_by('first_name', 'last_name')

    if item.type_id in (TYPE_ID_TAB, TYPE_ID_SECTION):
        layout = 'cols_content_panel'
    else:
        layout = 'cols_content_panel'

    return render(request, 'vanilla/item_form.html',
                  {'form': form,
                   'page_title': 'Edit',
                   'layout': layout,
                   'ARCHIVE_OPTION': ARCHIVE_OPTION})


@require_http_methods(["GET"])
@login_required(login_url=STAFF_LOGIN_URL)
@user_passes_test(lambda u: u.is_staff, login_url=STAFF_LOGIN_URL)
def tools_content(request, mode):
    """Tools Content list all pages by live, archived, draft, etc.

    TODO - sliced to 50 due to performance issue. Fix model functions to be more efficient.
    """
    if mode == 'expired':
        page_title = 'Pages past review'
        items = Item.objects.past_review_date()
    elif mode == 'draft':
        page_title = 'Draft pages'
        items = Item.objects.draft()
    elif mode == 'pending':
        page_title = 'Pages before publish date'
        items = Item.objects.before_publish_date()
    elif mode == 'current':
        page_title = 'Current pages'
        items = Item.objects.live()
    elif mode == 'archived':
        page_title = 'Archived pages'
        items = Item.objects.archived()
    else:
        page_title = 'Pages for approval'
        items = Item.objects.for_approval()

    paginator = Paginator(items, MAX_ITEMS_PER_PAGE)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    try:
        items_on_page = paginator.page(page)
    except (EmptyPage, InvalidPage):
        items_on_page = paginator.page(paginator.num_pages)

    return render(request, 'vanilla/tools_content.html',
                  {'layout': 'cols_menu_content',
                   'page_title': page_title,
                   'body_class': 'overall-sec-7',
                   'items': items_on_page,
                   'pages_for_approval': Item.objects.for_approval().count(),
                   'pages_past_review': Item.objects.past_review_date().count(),
                   'pages_draft': Item.objects.draft().count(),
                   'pages_before_publish': Item.objects.before_publish_date().count(),
                   'pages_live': Item.objects.live().count(),
                   'pages_archived': Item.objects.archived().count()})


@require_http_methods(["GET"])
@login_required(login_url=STAFF_LOGIN_URL)
@user_passes_test(lambda u: u.is_staff, login_url=STAFF_LOGIN_URL)
def tools_access(request):
    """List all staff members."""
    if 'inactive' in request.GET:
        page_title = 'Access: Inactive'
        members = Member.objects.inactive()
    else:
        page_title = 'Access: Current'
        members = Member.objects.live()
    return render(request, 'vanilla/tools_access.html', {
        'page_title': page_title,
        'members': members,
        'people_inactive': Member.objects.inactive().count(),
        'people_live': Member.objects.live().count(),
        'add_button': request.user.is_superuser,
        'add_action': reverse('vanilla:new_member')})


@require_http_methods(["GET", "POST"])
@login_required(login_url=STAFF_LOGIN_URL)
@user_passes_test(lambda u: u.is_staff, login_url=STAFF_LOGIN_URL)
def tools_member_edit(request, user_id):
    """Staff member editing."""
    if user_id is None:
        user = None
        member = None
    else:
        user = User.objects.get(id=user_id)
        member = Member.objects.get(user=user)

    if not request.user.is_superuser:
        return HttpResponseForbidden("Sorry, you are not allowed to edit this.")

    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=user)
        member_form = MemberForm(request.POST, instance=member)
        if user_form.is_valid() and member_form.is_valid():
            user = user_form.save()
            member = member_form.save(commit=False)
            member.user = user
            member.save()
            return redirect('vanilla:tools_access')
    else:
        user_form = UserForm(instance=user)
        member_form = MemberForm(instance=member)

    return render(request, 'vanilla/tools_member_edit.html', {
        'user_form': user_form,
        'member_form': member_form,
        'member': member,
        'cancel_url': reverse('vanilla:tools_access')})


def style_samples(request):
    return render(request, 'vanilla/style_samples.html')


class NewsListView(ListView):
    # Consider modifying this view to subclass ItemView. That would avoid the
    # hard-coded left-hand menu and right-hand panel.
    queryset = Item.objects.news()
    template_name = 'vanilla/news_list.html'
    paginate_by = 7

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Temporary left-hand menu items.
        context['sub_menu'] = [
            {'title': 'Contact us', 'get_absolute_url': '/general/media/432/'},
            {'title': 'Media releases', 'get_absolute_url': '/general/media/405/'},
        ]

        # Hook up the "add" button to add a new News item, not new sub page.
        news_parent_item = get_object_or_404(
            Item, pk=settings.VANILLA_CONFIG['NEWS_PARENT_ITEM'])
        context.update({
            'add_button': news_parent_item.can_add_below(self.request.user),
            'add_action': reverse(
                'vanilla:create_item', kwargs={
                    'parent_item_id': settings.VANILLA_CONFIG['NEWS_PARENT_ITEM']}),
        })
        return context


class ItemView(TemplateView):
    """Class-based replacement for previous show_page + render_item functions.

    This helps to give us finer control over special cases where we want to
    modify the context data or template for a particular item while retaining
    the existing functionality. For example, you could subclass this view to
    build a a page that has part custom generated content as well as the
    standard editable columns.

    """
    def get_template_names(self):
        if self.template_name:
            return [self.template_name]
        else:
            return [self.object.which_template()]

    def get_context_data(self, section=None, subsection=None, overall_sec='overall-sec-1'):
        # Build body class.
        body_class = '{overall_sec} lvl-{level} item-{item}'.format(
            overall_sec=overall_sec,
            level=self.object.type_id,
            item=self.object.item_id)
        if section:
            body_class += ' main-sec-'+ section.slug()
        if subsection:
            body_class += ' sub-sec-' + subsection.slug()

        # Sanitise HTML, ie. remove Javascript.
        if not self.object.allow_content_js:
            self.object.strip_js()

        # Permissions
        (edit_action, add_action) = get_staff_buttons_action(self.request, self.object)
        can_edit = self.object.can_edit(self.request.user, self.request.session.session_key)
        can_add = self.object.can_add_below(self.request.user)

        # Live or pending content.
        page_contents = self.object.get_page_contents(staff=self.request.user.is_staff)

        # Get menus for a page.
        #
        # TODO: main_menu is done through a template tag. Probably don't need to
        # build it in get_page_menus. Wonder if same could be done for others?
        (_, sub_menu, this_section_menu, page_menu) = \
            self.object.get_page_menus(self.request.user.is_staff, self.request.session.session_key)

        # Home page?
        home = self.object.item_id == ITEM_ID_HOME

        # Section level page?
        sectionlevel = self.object.type_id == TYPE_ID_SECTION

        # Simpler title if home page.
        if home:
            page_title = ''
        else:
            page_title = page_contents['title']

        section_title = None
        if section:
            section_title = section.extract_sort_num()[1]

        parent_title = None
        parent_href = None
        if self.object.relate:
            parent_title = self.object.relate.extract_sort_num()[1]
            parent_href = self.object.relate.get_absolute_url()

        return {
            'page_id': self.object.item_id,
            'item_instance': self.object,
            'item_type': self.object.type_id,
            'page_title': page_title,
            'page_desc': self.object.seo_description or self.object.summary,
            'page_href': self.object.get_absolute_url(),
            'sub_menu': sub_menu,
            'this_section_menu': this_section_menu,
            'page_menu': page_menu,
            'page_summary': page_contents['summary'],
            'main_content_html': page_contents['content'],
            'sub_content_b_html': page_contents['sub_content_b'],
            'parent_title': parent_title,
            'parent_href': parent_href,
            'home_layout': self.object.use_home_layout(),
            'home': home,
            'sectionlevel': sectionlevel,
            'body_class': body_class,
            'add_action':  add_action,
            'edit_action': edit_action,
            'edit_button': can_edit,
            'add_button' : can_add,
            'site': Site.objects.get_current(),
            'section': section,
            'section_title': section_title,
            'subsection': subsection}

    def get(self, request, tab, slug=None, id=None):
        """Convert from the URL tab, slug and/or id to the relavent item.

        Handles either a tab, slug and id; a tab and slug; or just a tab.

        Public can't view "new" or "archived" pages at all. Public can view any
        page that has a live and a draft version, but they only see the live
        version.

        """
        tab_slug = tab
        section_slug = slug

        item = section = tab = None

        # First we'll try to look up the tab, section and id given in the URL.
        if tab_slug:
            tab_slug = tab_slug.replace('&', 'and').strip()
            # TODO: Make slug a actual field with a unique constraint and index
            # for fast lookups.
            tabs = Item.objects.filter(type_id=TYPE_ID_TAB)
            if not request.user.is_staff:
                tabs = tabs.exclude(archived=True)
            for tab in tabs:
                if (tab.slug() == tab_slug and
                        (tab.is_published() or request.user.is_staff)):
                    break
            else:
                raise Http404('No such tab found: {}'.format(tab_slug))

        # Make sure sections are in the tab above in case the names aren't unique.
        if section_slug:
            section_slug = section_slug.replace('&', 'and').strip()
            sections = Item.objects.filter(type_id=TYPE_ID_SECTION, relate=tab)
            if not request.user.is_staff:
                sections = sections.exclude(archived=True)
            for section in sections:
                if (section.slug() == section_slug and
                        (section.is_published() or request.user.is_staff)):
                    break
            else:
                raise Http404('No such section found: {}'.format(section_slug))

        if id:
            if request.user.is_staff:
                item = get_object_or_404(Item, pk=id)
            else:
                item = get_object_or_404(Item, pk=id, archived=False)
                if not item.is_published():
                    raise Http404('No such item found: {}'.format(id))

        # Before we render the page, we need to ensure that the URL's
        # id, section and tab match up. Otherwise you might be able to cheat and
        # access a restricted page by specifying a different tab or section.
        #
        # Perhaps the section and subsection shouldn't be passed through to
        # get_context_data; they should just be looked up there. This was
        # originally done for efficiency.
        item_section = item_subsection = None
        if item:
            (item_tab, item_section, item_subsection, item_topic) = item.get_ancestors()
            if item_tab != tab or item_section != section:
                raise Http404('Wrong tab or section for this item.')

            # Check that the other parent pages are not archived. Tab and
            # Section are already done above.
            if item_subsection and item_subsection.archived:
                raise Http404('Parent sub-section page is archived.')
            if item_topic and item_topic.archived:
                raise Http404('Parent topic page is archived.')

            self.object = item
        elif section:
            # From above, we already know the section is within the tab, so no
            # need to check.
            self.object = section
        else:
            self.object = tab

        if self.object.item_id in NSW_REDIRECT_IDS and not self.request.user.is_staff:
            ip = get_real_ip(request)
            if ip is None:
                ip = get_ip(request)
            if in_NSW(ip, os.path.join(os.path.dirname(__file__), 'GeoLiteCity.dat')):
                return redirect('/general/membership/4655/', permanent=True)

        # Pages with member_only set to true should require the user be logged in
        # and only be accessible by staff and current members.
        if self.object.member_only:
            if request.user.is_authenticated():
                if not request.user.is_staff:
                    person = request.user.person
                    if not person.current_membership() and not person.is_member_club():
                        # No page for you!
                        #
                        # We could have avoided this additional nesting by not
                        # using a variable for person, but this helps with
                        # clarity.
                        return render(request, 'vanilla/members_only.html',
                                      {'layout': 'cols_content'},
                                      status=403)
            else:
                return redirect_to_login(next=self.object.get_absolute_url())

        # Render the page.
        if self.object.redirect_to:
            return redirect(self.object.redirect_to, permanent=True)
        else:
            context = self.get_context_data(item_section, item_subsection)
            return self.render_to_response(context)


class HomeItemView(ItemView):
    """Custom home page with embedded news feed."""

    template_name = 'vanilla/home.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['news_list'] = Item.objects.news()
        return context


# Local variables:
# coding: utf-8
# mode: python
# End:
# vim: fileencoding=utf-8 filetype=python :
