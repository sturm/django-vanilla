from django.contrib import admin

from vanilla.models import Item

class ItemAdmin(admin.ModelAdmin):
    list_display = ['item_id', '__str__', 'type', 'archived', 'user', 'publish_date']
    search_fields = ['title', 'title_pending']
    raw_id_fields = ['user', 'relate']
    list_filter = ['type', 'archived', 'member_only', 'allow_content_js']

admin.site.register(Item, ItemAdmin)
