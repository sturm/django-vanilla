# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0004_auto_20150429_2350'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='allow_content_js',
            field=models.BooleanField(verbose_name='Allow unsafe HTML', db_column='ItemAllowContentJS', default=False),
        ),
    ]
