# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0007_auto_20150717_2306'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='edit_mode_id',
            field=models.IntegerField(choices=[(1, 'approved'), (2, 'for approval'), (3, 'draft')], default=3, db_column='ItemEditModeId'),
        ),
    ]
