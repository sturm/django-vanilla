# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0006_auto_20150601_1531'),
    ]

    operations = [
        migrations.AlterField(
            model_name='member',
            name='user',
            field=models.OneToOneField(on_delete=models.CASCADE, primary_key=True, to=settings.AUTH_USER_MODEL, serialize=False),
        ),
    ]
