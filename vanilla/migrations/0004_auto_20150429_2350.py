# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vanilla', '0003_auto_20150422_0040'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photo',
            name='item',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_send_photo',
        ),
        migrations.RemoveField(
            model_name='item',
            name='show_send_photo_pending',
        ),
        migrations.DeleteModel(
            name='Photo',
        ),
    ]
