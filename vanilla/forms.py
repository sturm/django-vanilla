from django import forms
from django.contrib.auth.models import User

from .models import Item, Member, \
    EDIT_MODE_APPROVE, EDIT_MODE_READY, EDIT_MODE_DRAFT, \
    STATUS_ID_NEW, STATUS_ID_PENDING

ARCHIVE_OPTION = 4

class ItemEditForm(forms.ModelForm):
    error_css_class = 'error'

    status = forms.TypedChoiceField(
        choices=((EDIT_MODE_DRAFT, 'Draft'),
                 (EDIT_MODE_READY, 'For approval'),
                 (EDIT_MODE_APPROVE, 'Live')),
        initial=EDIT_MODE_DRAFT,
        coerce=int)

    pub_review = forms.ChoiceField(
        choices=(('Publish', 'Publish'),
                 ('Review', 'Review')),
        initial='Publish')

    publish_date = forms.DateField(input_formats=['%d/%m/%Y', '%d-%m-%Y'],
                                   widget=forms.DateInput(format="%-d/%-m/%Y"),)
    change_summary_append = forms.CharField(required=False,
                                            max_length=200,
                                            widget=forms.TextInput(attrs={'placeholder': 'Changed page text...'}))

    class Meta:
        model = Item
        fields = ('title_pending', 'illustration',
                  'user', 'publish_date',
                  'summary_pending',
                  'content_pending', 'show_sub_content_b_pending',
                  'sub_content_b_pending')

    def __init__(self, can_publish=False, can_archive=False, *args, **kwargs):
        # Lower-level pages can be archived through the form,
        # upper-level pages can't.
        super().__init__(*args, **kwargs)
        self.fields['status'].choices = [(EDIT_MODE_DRAFT, 'Draft'),
                                         (EDIT_MODE_READY, 'For approval')]
        if can_publish:
            self.fields['status'].choices.append((EDIT_MODE_APPROVE, 'Live'))
        if can_archive:
            self.fields['status'].choices.append((ARCHIVE_OPTION, 'Archived'))
        if self.instance.archived:
            self.fields['status'].initial = ARCHIVE_OPTION
        self.initial['change_summary'] = '' # clear change summary, it is appended to each save

    def save(self, commit=True):
        item = super().save(commit=False)

        # HACK: This removes the hard-coded prefix of any link in the
        # content. Having a the http... means that people (like staff)
        # may be transitioned out of HTTPS inadvertently. This is
        # especially significant now we use secure cookies.
        # TODO: Generalise this using Site app so it works on any site.
        # TODO: Perhaps move to a signal?
        try:
            item.content_pending = item.content_pending.replace('http://www.bv.com.au', '')
            item.sub_content_b_pending = item.sub_content_b_pending.replace('http://www.bv.com.au', '')
            item.content = item.content.replace('http://www.bv.com.au', '')
            item.sub_content_b = item.sub_content_b.replace('http://www.bv.com.au', '')
        except AttributeError:
            # HACK: Handle case where one of the above attributes is
            # None (so doesn't have a replace method).
            pass

        if self.cleaned_data['status'] == ARCHIVE_OPTION:
            item.archive(commit=False)
        else:
            item.archived = False
            item.edit_mode_id = self.cleaned_data['status']
            if self.cleaned_data['status'] == EDIT_MODE_APPROVE:
                item.publish(commit=False)
            elif item.item_id is None or item.status_id == STATUS_ID_NEW:
                item.status_id = STATUS_ID_NEW
                # Live title is currently used for slug, so we need it set on a
                # new page, even if not published yet.
                item.title = item.title_pending
            else:
                item.status_id = STATUS_ID_PENDING
        if commit:
            item.save()
        return item


class UserForm(forms.ModelForm):
    password = forms.CharField(
        label='New password', max_length=50, required=False,
        widget=forms.PasswordInput)
    error_css_class = 'error'

    class Meta:
        model = User
        fields = [
            'is_active', 'username', 'password', 'first_name', 'last_name',
            'email']

    def clean_change_password(self):
        """Password is required for a new staff member."""
        data = self.cleaned_data['password']
        if self.instance.user_id is None and data == '':
            raise forms.ValidationError("Please add a password.")
        return data

    def save(self, force_insert=False, force_update=False, commit=True):
        user = super().save(commit=False)
        user.is_staff = True
        if self.cleaned_data['password']:
            user.set_password(self.cleaned_data['password'])
        user.save()
        return user


class MemberForm(forms.ModelForm):
    error_css_class = 'error'

    class Meta:
        model = Member
        fields = ['role']
