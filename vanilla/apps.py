from django.apps import AppConfig

class VanillaConfig(AppConfig):
    name = 'vanilla'
    verbose_name = 'Vanilla Editable Website'
