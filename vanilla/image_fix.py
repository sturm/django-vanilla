"""Fix unquoted spaces in URLs.

Technically, spaces aren't allowed in URLs. Most browsers don't care about
this, but tools like linklint do.

Loop through all pages in website. For matching src and href attributes
values, replace spaces with %20. Multiple replacement runs are made as a
hack to handle URLs with multiple spaces (there's no doubt a neater way).

TODO: Rewrite as a Django management command.

"""
# Hack to run it as if inside parent dir
import sys
from os.path import dirname, abspath
sys.path[0] = dirname(dirname(abspath(__file__)))

from django.core.management import setup_environ
import settings
setup_environ(settings)

from vanilla.models import Item
import re

space_in_src = re.compile('src="([^"]*) ([^"]*?)"')
space_in_href = re.compile('href="([^"]*) ([^"]*?)"')

def fix_urls_in_text(text):
    if text is not None and len(text) > 0:
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
        text = space_in_src.sub(r'src="\1%20\2"', text)
    if text is not None and len(text) > 0:
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
        text = space_in_href.sub(r'href="\1%20\2"', text)
    return text


items = Item.objects.all()
for item in items:
    print(item.title)
    item.content = fix_urls_in_text(item.content)
    item.sub_content_b = fix_urls_in_text(item.sub_content_b)
    item.content_pending = fix_urls_in_text(item.content_pending)
    item.sub_content_b_pending = fix_urls_in_text(item.sub_content_b_pending)
    item.save()
