from django.conf import settings
from django.contrib.syndication.views import Feed

from .models import Item

class NewsFeed(Feed):
    """News Feed similar to the Latest News section of the website."""

    title = "{org_name} News".format(org_name=settings.ORG_NAME)
    link = "/"

    def items(self):
        return Item.objects.news()

    def item_title(self, item):
        return item.__str__()

    def item_link(self, item):
        return item.get_absolute_url()

    def item_description(self, item):
        return item.content
