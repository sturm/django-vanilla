from django import template

from ..models import Item, ITEM_ID_ROOT

register = template.Library()

@register.inclusion_tag('vanilla/main_menu_tag.html')
def main_menu(item=None, staff_menu=False):
    """Display the main menu.

    Args:
     - item: Display the menu with respect to this Item. If not specified, use
             the default tab.

    """
    if item:
        return {'items': item.main_menu(staff_menu)}
    else:
        try:
            items = Item.objects.get(pk=ITEM_ID_ROOT).main_menu(staff_menu)
        except Item.DoesNotExist:
            items = None
        return {'items': items}

@register.inclusion_tag('vanilla/main_menu_dynamic_tag.html')
def main_menu_dynamic(item=None, staff_menu=False):
    """Display the main menu with drop down menus for each item.

    Args:
     - item: Display the menu with respect to this Item. If not specified, use
             the default tab.

    """
    if item:
        items = item.main_menu(staff_menu)
    else:
        try:
            items = Item.objects.get(pk=ITEM_ID_ROOT).main_menu(staff_menu)
        except Item.DoesNotExist:
            items = None
    subitems = []
    if items:
        for i in items:
            subitems.append(i.sub_menu)
    else:
        subitems.append(None)
    return {'items': items, 'subitems': subitems}


@register.inclusion_tag('vanilla/main_menu_mobile_tag.html')
def main_menu_mobile(item=None, staff_menu=False):
    """Display the main menu mobile version

    Args:
     - item: Display the menu with respect to this Item. If not specified, use
             the default tab.

    """
    if item:
        items = item.main_menu(staff_menu)
    else:
        try:
            items = Item.objects.get(pk=ITEM_ID_ROOT).main_menu(staff_menu)
        except Item.DoesNotExist:
            items = None
    subitems = []
    if items:
        for i in items:
            subitems.append(i.sub_menu)
    else:
        subitems.append(None)
    return {'items': items, 'subitems': subitems}
