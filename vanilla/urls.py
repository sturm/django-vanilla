from django.conf.urls import patterns, url
from django.views.generic.base import RedirectView

from .views import HomeItemView, ItemView
from .feeds import NewsFeed

app_name = 'vanilla'
urlpatterns = patterns(
    'vanilla.views',
    (r'^search/$', 'search'),
    (r'^add/(?P<parent_item_id>\d+)/$', 'create_item'),
    (r'^edit/(?P<item_id>\d+)/$', 'update_item'),
    (r'^tools/content/(?P<mode>\w+)/$', 'tools_content'),
    (r'^tools/access/$', 'tools_access'),
    (r'^tools/member-edit/(?P<user_id>-?\d+)/$', 'tools_member_edit'),
    url(r'^tools/member-edit/$', 'tools_member_edit', {'user_id': None},
        name='new_member'),

    # Style guide
    (r'^style-samples/$', 'style_samples'),

    # Home
    url(r'^$', HomeItemView.as_view(), {'tab': 'general'}, name='home'),
    url(r'^general/$', RedirectView.as_view(url='/', permanent=True)),

    # News feed
    url(r'^news/', NewsFeed(), name='news_feed'),

    # Regular content pages.
    url(r'^(?P<tab>[\w-]+)/$', ItemView.as_view(), name='vanilla.views.show_page'),
    url(r'^(?P<tab>[\w-]+)/(?P<slug>[\w&-]+)/$', ItemView.as_view(), name='vanilla.views.show_page'),
    url(r'^(?P<tab>[\w-]+)/(?P<slug>[\w&-]+)/(?P<id>\d+)/$', ItemView.as_view(), name='vanilla.views.show_page'),
)
