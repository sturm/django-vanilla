"""A setuptools based setup module.

Based on https://github.com/pypa/sampleproject.

"""
from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    with open(path.join(here, 'HISTORY.rst'), encoding='utf-8') as g:
        long_description = f.read() + '\n\n' + g.read()

setup(
    name='django-vanilla',
    version='0.7.21',
    description='A simple Django-based content management system.',
    long_description=long_description,
    url='https://gitlab.com/sturm/django-vanilla',
    author='Ben Sturmfels',
    author_email='ben@sturm.com.au',
    license='Apache License, Version 2.0',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Framework :: Django',
    ],
    packages=find_packages(exclude=['contrib', 'docs', 'tests*']),
    package_data={'vanilla': ['GeoLiteCity.dat']},
    install_requires=[
        'Django>=1.8',
        'bleach>=1.4.1',
    ],
    extras_require={
        'test': [
            'django_testscenarios>=0.7.2',
        ],
    },
)
