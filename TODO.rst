=======
 To Do
=======

High priority
-------------

 * Add explicit unique slugs to database and use these for fast lookups.
 * Use slugs in all URLs, rather than some integer IDs.
 * Reduce the excessive number of duplicate Item queries made when reversing
   URLs for menus.
 * Archiving a page should redirect to the parent, currently returns 404.


Low priority
------------

 * Reduce number of Item queries when displaying a page
 * Allow switching between draft and live versions.
 * Allow dismissing draft changes.
 * Interface to allow pages to be moved around.
 * Full-text search with Haystack/Whoosh or ElasticSearch
 * Use a page tree model that allows more efficient lookup, eg. try
  ``django-treebeard`` or ``django-mptt``, see also
  http://www.slideshare.net/davidfetter/tree-tricks-osdcmelbourne20101124
 * Allow subclassing of Item to add additional fields like maps
 * prevent loss of work, even if logged out before saving, see
   http://dev.w3.org/html5/webstorage/#introduction
 * Signal handler to post-process HTML upon save()
   * remove hard-coded references to own domain
   * tidy the HTML
 * Separate representation of pages from relationships between them


Ideas
-----

 * Separate "generated" version of content that is produced by post-processing hooks.
 * Hook functions to clean data, spelling, link checking, image resizing etc.
 * Various handy editing features:
   - add a sub-page here called ...
   - page content post-save-processing hooks
     - correct validation
     - spelling
   - revert this page
   - move a page
   - update page content
   - get content of page ...
